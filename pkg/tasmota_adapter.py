"""TP-Link adapter for Mozilla IoT Gateway."""

from gateway_addon import Adapter, Database
from .tasmota_device import TasmotaSonoffPowR2
import threading
import paho.mqtt.client as mqtt

class TasmotaAdapter(Adapter):
    def __init__(self, mqttbroker, verbose=False):
        self.name = self.__class__.__name__
        self.mqttc = mqtt.Client()
        self.mqttc.connect(mqttbroker)
        t = threading.Thread(target=self.mqttc.loop_start())
        t.daemon = True
        t.start()
        dev = TasmotaSonoffPowR2(self, 'test', topic="sonoff_heater_livingroom")
        Adapter.__init__(self, 'tasmota-adapter', 'tasmota-adapter', verbose=verbose)
        self.handle_device_added(dev)