from gateway_addon import Device
import json

class TasmotaDevice(Device):
    """Common class for Tasmota devices."""

    def __init__(self, adapter, _id, topic="sonoff"):
        Device.__init__(self, adapter, _id)
        self.topic = topic
        self.adapter.mqttc.on_connect = self.on_connect
        self.adapter.mqttc.on_message = self.on_message
        self.discover()

    def on_message(self, client, userdata, msg):
        p = str(msg.payload, encoding='utf-8')
        try:
            j=json.loads(p)
            self.properties.update(j)
            print(self.properties)
        except ValueError:
            return p

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe('+/{}/+'.format(self.topic))

    def _publish(self, topic, payload):
        self.adapter.mqttc.publish(topic, payload)

    def discover(self):
        self._publish('cmnd/{}/STATUS'.format(self.topic), 0)

class TasmotaSonoffPowR2(TasmotaDevice):
    def __init__(self, adapter, _id, topic):
        TasmotaDevice.__init__(self, adapter, _id, topic)

    @staticmethod
    def voltage():
        pass
