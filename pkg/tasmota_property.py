from gateway_addon import Property

class TasmotaProperty(Property):
    """Tasmota common Property class"""

    def __init__(self, device, name, description, value):
        Property.__init__(self, device, name, description)
        self.set_cached_value(value)

class TasmotaSonoffPowR2(TasmotaProperty):
    def set_value(self, value):
        pass